import React, { Component } from "react";
import todosList from "./todos.json";

class App extends Component {
  state = {
    todos: todosList,
  };
  handleTodo =(event)=>{
    if(event.key==="Enter"){
      let addTodo ={
        userId: 1,
        id: Math.random()*1000,
        title: event.target.value,
        completed: false
      }
      let refreshTodo= [...this.state.todos,addTodo];
      this.setState({...this.state,todos:refreshTodo,value:""})
    }
  }

handleTodoUpdate =(event)=>{
  this.setState({...this.state,value:event.target.value})
}


handleComplete=(id)=>{
  let checkMark = this.state.todos.map((todo)=>{
    if(todo.id=== id){
      return{
        ...todo,
        completed:!todo.completed
      }
    }
    return todo
  });
  this.setState((state)=>{
    return{
      ...state,
      todos: checkMark
    }
  })
};


handleX=(event,id)=>{
  let deleteInfo= this.state.todos.filter((todo)=>todo.id !==id)
  this.setState({todos:deleteInfo})
}

deleteAll=(event)=>{
let deleteInfo = this.state.todos.filter((todo)=>todo.completed === false)
this.setState({todos:deleteInfo})
}
  render() {
    return (
      <section className="todoapp">
        <header className="header">
          <h1>todos</h1>
          <input 
          className="new-todo" 
          placeholder="What needs to be done?" 
          autofocus 
          onKeyDown={this.handleTodo}
          onChange= {this.handleTodoUpdate}
          value= {this.state.value}
          />
        </header>
        <TodoList 
        todos={this.state.todos}
        handleComplete={this.handleComplete}
        handleX={this.handleX}
        deleteAll ={this.deleteAll}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button 
          className="clear-completed"
          onClick={this.deleteAll}
          >Clear completed</button>
        </footer>
      </section>
    );
  }
}

class TodoItem extends Component {
  render() {
    return (
      <li className={this.props.completed ? "completed" : ""}>
        <div className="view">
          <input 
          className="toggle" 
          type="checkbox" 
          checked={this.props.completed} 
          onChange={this.props.handleComplete}
          />
          <label>{this.props.title}</label>
          <button className="destroy" 
          onClick={this.props.handleX}
          />
        </div>
      </li>
    );
  }
}

class TodoList extends Component {
  render() {
    return (
      <section className="main">
        <ul className="todo-list">
          {this.props.todos.map((todo) => (
            <TodoItem 
            key={todo.id}
            title={todo.title} 
            completed={todo.completed} 
            handleComplete={(event)=>this.props.handleComplete(todo.id)}
            handleX={(event)=>this.props.handleX(event,todo.id)}
            />
          ))}
        </ul>
      </section>
    );
  }
}

export default App;
